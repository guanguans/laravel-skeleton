<?php

namespace App\Http\Controllers\Api;

use App\Traits\WithJsonResponse;

class Controller extends \App\Http\Controllers\Controller
{
    use WithJsonResponse;
}
